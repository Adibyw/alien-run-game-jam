extends KinematicBody2D

export (int) var speed = 100
export (int) var timer = 60

export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed

func speed_timer():
	if timer != 0:
		timer -= 1
	else:
		timer = 60
		speed += 30
		

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
	velocity.x = 0
	velocity.x += speed
	speed_timer()

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	else:
		animator.play("Walk")
