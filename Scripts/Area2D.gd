extends Area2D

export (String) var sceneName = "Game Over"

func _on_Area_Trigger_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
			get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
